import facebook as fb
from os import listdir
from os.path import isfile, join
import datetime
import time
import shutil

#check start time 
access_token = "EAAxxZBSsGAQYBAHFFtaG5bZB6fPWA5ZBql2TdZAaZBohlkftkOWYKjw63ZARrZBAD1GZBop5odBZCNQPVm3Ukj0tCMdDkNV9TSoSqDcQCwluzL97bT3N0ak5OdKOzh7nrC2O8LzKULaUGwcZATrPMUvjN25ZCZB4elwaozoiDhpQHiEyso8kRcW2rFIbTzjquJAUkcEy5aWsZAsVpSj7Uvl7DQXnZBAvL6i2tw6LMZD"
allowed_image_extension = ['png','jpeg','jpg']
month = 5
day = 8
time_slots = [10,4,4,3]
start_slot = 0
default_fp = "images/"
posted_fp = "done_images/"
_message = 'For more such content follow us. Like and share to help us grow. Thank You.                       #funny #memes #meme #funnymemes #lol #dankmemes #love #fun #comedy #follow #memesdaily #like #instagram #humor #instagood #tiktok #funnyvideos #lmao #dank #cute #dailymemes #bhfyp #edgymemes #jokes #laugh #offensivememes #explorepage #memepage #happy #bhfyp'

def post_to_facebook(asafb,file_path,seconds_since_epoch):
    res = asafb.put_photo(open(f"{default_fp}{file_path}","rb"), message = _message, published = False , scheduled_publish_time=int(seconds_since_epoch), unpublished_content_type="SCHEDULED")
    if res["id"] != None:
        print("Success")
        shutil.move(f"{default_fp}{file_path}", f"{posted_fp}{file_path}")

def start_file_upload_process(mypath = "images/"):
    asafb = fb.GraphAPI(access_token)
    # page_details = asafb.get_object("me")
    # page_id = page_details['id']

    if asafb == None:
        print("Error Fetching Token")
        return
    start_index = start_slot
    seconds_since_epoch = (datetime.datetime(2021,month,day,0,0)).timestamp()
    for f in listdir(mypath):
        if(isfile(join(mypath, f)) and f.split('.')[1] in allowed_image_extension):
            if(start_index > 3):
                start_index = 0
            
            seconds_since_epoch = (datetime.datetime.fromtimestamp(seconds_since_epoch) + datetime.timedelta(hours = time_slots[start_index])).timestamp()
            post_to_facebook(asafb,f,seconds_since_epoch)
            start_index = start_index + 1
            

if __name__ == "__main__":
    start_file_upload_process()